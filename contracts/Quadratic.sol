pragma solidity >=0.5.7 <0.7.0;

import "abdk-libraries-solidity/ABDKMath64x64.sol";

contract Quadratic {
    // Solve for x where a, b and c are given.
    // EXCLAIMER: Do not use this in a real contract.
    function solveForX(int256 a, int256 b, int256 c) pure public returns (int256[] memory) {
        int256[] memory xValues = new int256[](2);

        int256 alpha = (b * b) - 4 * a * c;

        if (alpha == 0) {
            xValues[0] = (-1 * b) + alpha;
        } else if (alpha > 0) {
            int128 sqrtAlpha = ABDKMath64x64.toInt(ABDKMath64x64.sqrt(ABDKMath64x64.fromInt(alpha)));
            xValues[0] = ((-1 * b) + sqrtAlpha) / 2 * a;
            xValues[1] = ((-1 * b) - sqrtAlpha) / 2 * a;
        }

        return xValues;
    }
}
