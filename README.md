# Smart Contracts that cover UWA MATH1720 Mathematics Fundamentals

These contracts cover what I have been learning in MATH1720. I figured I could
try and extend my knowledge of the unit by trying to convert the mathematical
calculations into Ethereum-based smart contracts.

I created these smart contracts because I was bored an figured I needed a
change from practising unit questions over and over. It seems having to
program quadratic expressions and solving for X is a really good way to
memorize.

## Getting Started

Firstly, you need some kind of understanding of how contracts work and you'll
need some command line experience.

If you want to learn more about building Ethereum smart contracts start by checking out:

- https://ethereum.org/what-is-ethereum/
- https://solidity.readthedocs.io/
- https://www.trufflesuite.com/

If you're really new to blockchain, decentralized ledgers and cryptocurrency, do some searching. You can search for things such as "what is cryptocurrency", "bitcoin", or "what is blockchain".

Lastly, you'll need some basic understanding of Javascript.

## Running the contracts

So far I have not created any interface. You'll have to run the contracts from the console using Truffle.

Here are some simple commands to get up and running (you'll need to have Truffle running and this code cloned to your machine to continue):

```
# cd /path/to/cloned/repo/
# npm i
# truffle develop
```

Once up and running in truffle develop:

```
truffle(develop)> const BN = require("bn.js")
truffle(develop)> deploy
truffle(develop)> var quadratic = await Quadratic.new()
truffle(develop)> var result = await quadratic.solveForX(1, -5, 4)
truffle(develop)> new BN(result[0]).toString()
truffle(develop)> new BN(result[1]).toString()
```

So far I have only developed one contract for solving quadratic expressions. I may write more, depends on time. If you want to fork the repo and extend this by covering other concepts from MATH1720, feel free.

Some limitations of the solveForX function:

- It can't handle fractions in,
- It probably can't handle fractions out,
- It definitely can't leave results in surds form.

solveForX only returns an array of x values. No values if alpha is < 0, one value if alpha == 0 and two values if x > 0.

## What's the point?

Trying to build a program to carry out a mathematical calculation is a a great way to learn how to do it and gives you an understanding beyond just solving it. It's similar to trying to teach or explain a solution to a mathematical problem; you really have to explain your answer and set out a logical solution.

Mathematical modelling and blockchain technologies go really well together. If you want to find out more about how maths can be applied to solving real world problems, check out https://ethgasstation.info/blog/quadratic-funding-in-a-nutshell/ where they use quadratics to make voting and funding more fair and less susceptible to corruption.

## DISCLAIMER

This smart contract is not secure in any way and has not been audited. Only use for testing and development.
